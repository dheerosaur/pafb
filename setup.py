import os
from setuptools import setup, find_packages

setup(
    name='django-filebrowser',
    version='0.1',
    description='Media-Management with Grappelli',
    url='http://bitbucket.org/dheerosaur/pafb',
    download_url='',
    packages=find_packages(),
    include_package_data=True,
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
    ],
    zip_safe=False,
)
